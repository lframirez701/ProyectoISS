create database ProyectISS;
use ProyectISS;

create table Estate(
IdEstate bigint primary key not null,
NameEstate varchar(30) not null
);
create table TypeFile(
IdTypeFile bigint primary key not null,
NameTypeFile varchar(45)
);

create table TypeTrainingProgram(
IdTypeTrainingProgram bigint primary key not null,
NameTypeTrainingProgram varchar(60) not null
);

Create table TypeFormationArea (
IdTypeFormationArea bigint primary key not null,
NameType varchar(55) not null
);
create table Phone(
IdPhone bigint primary key not null,
phonenumber bigint not null
);

create table WorkingDay(
IdWorkingDay bigint primary key not null,
WorkingDayName varchar(50),
StartTime Time not null,
FinishTime Time not null
);

create table Rol(
Idrol bigint primary key not null,
NameRol bigint not null,
SecondRol bigint
);

create table UserP(
IdUser bigint primary key not null,
Password varchar(30) not null,
fkRol bigint not null,
Constraint fk_Rol Foreign key (fkRol) references Rol(IdRol)
);

create table TrainingProgram(
IdTrainingProgram bigint primary key not null,
NameTrainingProgram varchar(70) not null,
fkTypeTrainingProgram bigint not null,
Constraint fk_TypeTrainingProgram Foreign key (fkTypeTrainingProgram) references TypeTrainingProgram(IdTypeTrainingProgram)
);

create table Filee(
IdFile bigint primary key not null,
ApprenticesNumber bigint  not null,
fkEstate bigint not null,
fkTypeFile bigint not null,
fkTrainingProgram bigint not null,
Constraint fk_Estate Foreign key (fkEstate) references Estate(IdEstate),
Constraint fk_TypeFile Foreign key (fkTypeFile) references TypeFile(IdTypeFile),
Constraint fk_TrainingProgram Foreign key (fkTrainingProgram) references TrainingProgram(IdTrainingProgram)
);

create table Person(
IdPerson bigint Primary key not null,
PersonNameVarchar varchar(45) not null,
DocumentationNumber bigint not null,
fkUserP bigint not null,
Address varchar(45) not null,
fkPhoneNumber bigint not null,
Email varchar(55) not null,
Locality varchar(40) not null,
Neigborhood varchar(50) not null,
Constraint fk_user Foreign key (fkUserP) references UserP(IdUser),
Constraint fk_Phone Foreign key (fkPhoneNumber) references Phone(IdPhone)
);

create table FormationArea(
IdFormationArea bigint primary key not null,
FormationName varchar(100) not null,
Address varchar(100) not null,
Location varchar(90) not null,
Neigborhood varchar(90) not null,
email varchar(200) not null,
FormationPlace varchar(25) not null,
fkNumberContact bigint not null,
fkTypeFormationArea bigint not null,
Constraint fk_NumberContact Foreign key (fkNumberContact) references Phone(IdPhone),
Constraint fk_TypeFormationArea Foreign key (fkTypeFormationArea) references TypeFormationArea(IdTypeFormationArea)
);

create table TeamFormationArea(
IdTeam bigint primary key not null,
FullName varchar(100) not null,
email varchar(100) not null,
position varchar(30) not null,
fkNumberContac bigint not null,
fkIdFormationAre bigint not null,
Constraint fk_NumberContac Foreign key (fkNumberContac) references Phone(IdPhone),
Constraint fk_IdFormationAre Foreign key (fkIdFormationAre) references FormationArea(IdFormationArea)
);

create table Programming(
IdProgramming bigint primary key not null,
HoursToWork bigint not null,
ActivityType varchar (30),
fkWorkingDay bigint not null,
fkPersonInstructor bigint not null,
fkFile bigint not null,
fkFormationArea bigint not null,
Constraint fk_WorkingDay Foreign key (fkWorkingDay) references WorkingDay(IdWorkingDay),
Constraint fk_IdPersonInstructor Foreign key (fkPersonInstructor) references Person(IdPerson),
Constraint fk_File Foreign key (fkFile) references Filee(IdFile),
Constraint fk_FormationArea Foreign key (fkFormationArea) references FormationArea(IdFormationArea)
);

